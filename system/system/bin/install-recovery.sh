#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:67108864:f14377871b9a96fd5f907f059e96bc2996ba082e; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:67108864:a294abca8a7e37b1f9710de56dc9cda2790307cd \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:67108864:f14377871b9a96fd5f907f059e96bc2996ba082e && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
